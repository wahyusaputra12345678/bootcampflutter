// Soal no 1 Membuat Kalimat
void main() {
  var word = 'dart';
  var second = 'is';
  var third = 'awesome';
  var fourth = 'and';
  var fifth = 'I';
  var sixth = 'love';
  var seventh = 'it!';
  // cetak
  print('$word $second $third $fourth $fifth $sixth $seventh');
}

// soal no 2 Mengurai Kalimat
// void main() {
//   var sentence = "I am going to be Flutter Developer";
//   var exampleFirstWord = sentence[0];
//   var exampleSecondWord = sentence[2] + sentence[3];
//   var thirdWord = sentence.substring(5, 10); // going
//   var fourthWord = sentence.substring(11, 13); // to
//   var fifthWord = sentence.substring(14, 16); // be
//   var sixthWord = sentence.substring(17, 24); // lakukan sendiri
//   var seventhWord = sentence.substring(25); // lakukan sendiri
//   // cetak
//   print('First Word: $exampleFirstWord');
//   print('Second Word: $exampleSecondWord');
//   print('Third Word: $thirdWord');
//   print('Fourth Word: $fourthWord');
//   print('Fifth Word: $fifthWord');
//   print('Sixth Word: $sixthWord');
//   print('Seventh Word: $seventhWord');
// }

// Soal no 3
// import 'dart:io';
// void main(List<String> args) {
//   print("nama depan : ");
//   var inputFirstName = stdin.readLineSync();
//   print("nama belakang : ");
//   var inputLastName = stdin.readLineSync();
//   print("nama lengkap anda adalah : $inputFirstName $inputLastName");
// }

// soal no 4
// void main() {
//   int a = 5;
//   int b = 10;
//   var perkalian = a * b;
//   var pembagian = a / b;
//   var penjumlahan = a + b;
//   var pengurangan = a - b;
//   print('perkalian: $perkalian');
//   print('pembagian: $pembagian');
//   print('penjumlahan: $penjumlahan');
//   print('pengurangan: $pengurangan');
// }
