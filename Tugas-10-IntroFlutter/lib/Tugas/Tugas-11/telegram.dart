import 'package:flutter/material.dart';

class Teleram extends StatefulWidget {
  const Teleram({Key? key}) : super(key: key);

  @override
  _TeleramState createState() => _TeleramState();
}

class _TeleramState extends State<Teleram> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Telegram'),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(Icons.search),
          )
        ],
      ),
    );
  }
}
