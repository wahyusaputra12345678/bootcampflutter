void main() {
  // Soal 1
  // teriak();
  // ===============
  // Soal 2
  // print(kalikan());
  // ===============
  // Soal 3
  // print(introduce());
  // ==============-
  // Soal 4
  print(faktor(6));
}

// Soal 1
// teriak() {
//   print('Teriak');
// }
// ==========================
// Soal 2
// kalikan() {
//   var num1 = 12;
//   var num2 = 4;
//   //
//   var hasil_kali = num1 * num2;
//   return hasil_kali;
// }
// ==========================
// Soal 3
// introduce() {
//   var name = 'Agus';
//   var age = '30';
//   var addres = 'Jln. Malioboro, Yogyakarta';
//   var hobby = 'Gaming';
//   //
//   var perkenalkan =
//       ('Nama saya $name , umur saya $age , alamat saya di $addres dan saya punya hobi yaitu $hobby');
//   return perkenalkan;
// }
// ==========================
// Soal 4
faktor(n) {
  if (n <= 0) {
    return 1;
  } else {
    return n * faktor(n - 1);
  }
}
