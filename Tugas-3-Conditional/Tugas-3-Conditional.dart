import 'dart:io';

void main() {
  // Soal 1 Ternary Operator
  // var inputText1 = stdin.readLineSync();
  // inputText1 == "y" || inputText1 != "t"
  //     ? print('Anda akan menginstal aplikasi dart')
  //     : print("Aborted");
  //==================================================
  // Soal 2 If , else , else if
  // final inputNama = stdin.readLineSync();
  // final inputPeran = stdin.readLineSync();
  // // jika penyihir
  // if (inputNama == inputNama && inputPeran == 'penyihir') {
  //   print("Hai $inputNama selamat datang di Dunia Werewolf");
  //   print(
  //       'Halo $inputPeran $inputNama kamu dapat melihat siapa yang menjadi werewolf!');
  // }
  // // jika warefolf
  // else if (inputNama == inputNama && inputPeran == 'warewolf') {
  //   print("Hai $inputNama selamat datang di Dunia Werewolf");
  //   print('Halo $inputPeran $inputNama Kamu akan memakan mangsa setiap malam!');
  // }
  // // jika guard
  // else if (inputNama == inputNama && inputPeran == 'guard') {
  //   print("Hai $inputNama selamat datang di Dunia Werewolf");
  //   print(
  //       'Halo $inputPeran $inputNama kamu akan membantu melindungi temanmu dari serangan werewolf');
  // }
  // // apabila cuman diisi nama
  // else if (inputNama == inputNama && inputPeran == '') {
  //   print('Halo $inputNama Pilih peranmu untuk memulai game!');
  // }
  // // kosong semua
  // else {
  //   print("Nama harus diisi!");
  // }
  //  ==============================
// Soal 3 Switch Case
  // final quotes = stdin.readLineSync();
  // switch (quotes) {
  //   case "senin":
  //     {
  //       print(
  //           'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja');
  //       break;
  //     }
  //   case "selasa":
  //     {
  //       print(
  //           'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
  //       break;
  //     }
  //   case "rabu":
  //     {
  //       print(
  //           'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
  //       break;
  //     }
  //   case "kamis":
  //     {
  //       print(
  //           'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
  //       break;
  //     }
  //   case "jumat":
  //     {
  //       print('Hidup tak selamanya tentang pacar.');
  //       break;
  //     }
  //   case "sabtu":
  //     {
  //       print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
  //       break;
  //     }
  //   case "minggu":
  //     {
  //       print(
  //           'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
  //       break;
  //     }
  // }
  //  =================
  // Soal 4 Tanggal Switch Case
  var tanggal = 21;
  var bulan = 1;
  var tahun = 1945;
  switch (bulan) {
    case 1:
      {
        print('$tanggal ' ' Januari ' ' $tahun');
        break;
      }
    case 2:
      {
        print('$tanggal ' ' Februari ' ' $tahun');
        break;
      }
    case 3:
      {
        print('$tanggal ' ' Maret ' ' $tahun');
        break;
      }
    case 4:
      {
        print('$tanggal ' ' April ' ' $tahun');
        break;
      }
    case 5:
      {
        print('$tanggal ' ' Mei ' ' $tahun');
        break;
      }
    case 6:
      {
        print('$tanggal ' ' Juni ' ' $tahun');
        break;
      }
    case 7:
      {
        print('$tanggal ' ' Juli ' ' $tahun');
        break;
      }
    case 8:
      {
        print('$tanggal ' ' Agustus ' ' $tahun');
        break;
      }
    case 9:
      {
        print('$tanggal ' ' September ' ' $tahun');
        break;
      }
    case 10:
      {
        print('$tanggal ' ' Oktober ' ' $tahun');
        break;
      }
    case 11:
      {
        print('$tanggal ' ' November ' ' $tahun');
        break;
      }
    case 12:
      {
        print('$tanggal ' ' Desember ' ' $tahun');
        break;
      }
  }
}
