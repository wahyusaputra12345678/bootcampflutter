void main() async {
  // Objek
  var h = Human();
  print('Luffy');
  print('Zoro');
  print('Killer');
  await h.getData();
  print(h.name);
}

class Human {
  // Field
  String name = 'nama character one piece';
  // Gunanya Future tidak akan langsung mereturn tapi ada jeda waktunya
  Future<void> getData() async {
    await Future.delayed(Duration(seconds: 3));
    name = 'Hilmy';
    print('get data [done]');
  }
}
