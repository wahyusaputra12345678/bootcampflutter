void main() {
  Segitiga rumus;
  double luasSegitiga;
  // objek
  rumus = new Segitiga();
  rumus.setengah = 0.5;
  rumus.alas = 20.0;
  rumus.tinggi = 30.0;
  luasSegitiga = rumus.hitungLuas();
  print(luasSegitiga);
}

class Segitiga {
  // field
  late double setengah;
  late double alas;
  late double tinggi;
  // method
  double hitungLuas() {
    return this.setengah * alas * tinggi;
  }
}
